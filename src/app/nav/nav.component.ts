import { Component } from '@angular/core';
import { BreakpointObserver, Breakpoints } from '@angular/cdk/layout';
import { Observable } from 'rxjs';
import { map, shareReplay } from 'rxjs/operators';
import { Location } from "@angular/common";
import { Router } from "@angular/router";
import { AuthService } from '../auth.service';

@Component({
  selector: 'app-nav',
  templateUrl: './nav.component.html',
  styleUrls: ['./nav.component.css']
})
export class NavComponent 
{
  title:string = "Angular-test A"
  isHandset$: Observable<boolean> = this.breakpointObserver.observe(Breakpoints.Handset)
    .pipe(
      map(result => result.matches),
      shareReplay()
    );

  LogOut()
  {
    console.log("in nav.ts - LogOut()")
    this.authService.LogOut();
  }

  constructor(private breakpointObserver: BreakpointObserver,
              location: Location,
              router: Router,
              public authService:AuthService
              ) 
  {
      router.events.subscribe(val =>
                              {
                                if (location.path() == "/login")
                                {
                                  this.title = 'Login';
                                }
                                else if (location.path() == "/signup")
                                {
                                  this.title = 'Sign Up';
                                }
                                else 
                                {
                                  this.title = "ExTest";
                                }
                              }
                          );   
  }

}
